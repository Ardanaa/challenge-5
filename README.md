# ERD
- https://dbdiagram.io/d/62654ca71072ae0b6adb018a

# Endpoint CRUD
- app.post("/cars", carsController.createCars);
- app.post("/cars/:id", carsController.updateCarsById);
- app.post("/deleteCar/:id", carsController.deleteCar);

# Endpoint EJS
- app.get("/", carsController.renderHome);
- app.get("/cars", (req, res) => { res.render('addCars') });
- app.get("/update/:id", carsController.renderCarById);
